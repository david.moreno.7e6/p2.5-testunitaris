import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_15KtTest{
    @Test
    fun check0segons(){
        assertEquals(1, oneSecondMore(0))
    }
    @Test
    fun check59seconds(){
        assertEquals(0, oneSecondMore(59))
    }
    @Test
    fun checkMiddleSecond(){
        assertEquals(30, oneSecondMore(29))
    }
}