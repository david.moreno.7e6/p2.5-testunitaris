import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_12KtTest{
    @Test
    fun negativeCelsius(){
        assertEquals(23.0, celsiusToFahrenheit(-5.0))
    }
    @Test
    fun positiveCelsius(){
        assertEquals(41.0, celsiusToFahrenheit(5.0))
    }
    @Test
    fun zeroCelsius(){
        assertEquals(32.0, celsiusToFahrenheit(0.0))
    }
}