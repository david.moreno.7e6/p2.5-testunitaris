import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_4KtTest{
    @Test
    fun bigNumber (){
        assertEquals(50000,calculArea(1000,50))
    }
    @Test
    fun lowNumber (){
        assertEquals(2, calculArea(2, 1))
    }
    @Test
    fun positiveNumbers (){
        assertEquals(20, calculArea(2, 10))
    }
}