import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_7KtTest {
    @Test
    fun negativeNumber(){
        assertEquals(-1, nextNumber(-2))
    }
    @Test
    fun positiveBigNumber(){
        assertEquals(10000, nextNumber(9999))
    }
    @Test
    fun checkZeroResult(){
        assertEquals(0, nextNumber(-1))
    }
}