import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_8KtTest{
    @Test
    fun negativeNumber(){
        assertEquals(-4.4, doubleDecimalValue(-2.2))
    }
    @Test
    fun positiveNumberInt(){
        assertEquals(2.0, doubleDecimalValue(1.0) )
    }
    @Test
    fun zeroDceimalNumber(){
        assertEquals(1.0, doubleDecimalValue(0.5))
    }
}