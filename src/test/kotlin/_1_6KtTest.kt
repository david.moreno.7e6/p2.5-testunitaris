import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_6KtTest{
    @Test
    fun bigNumbers(){
        assertEquals(1850, pupitresForClass(200,3000,500))
    }
    @Test
    fun positiveNumbers(){
        assertEquals(15, pupitresForClass(5,10,15))
    }
    @Test
    fun oddNumber(){
        assertEquals(16, pupitresForClass(5,10,16))
    }
}