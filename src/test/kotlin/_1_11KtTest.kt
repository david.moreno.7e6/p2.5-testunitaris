import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_11KtTest{
    @Test
    fun bigNumber(){
        assertEquals(10000.0, calcualVolumAire(100.0,10.0,10.0))
    }
    @Test
    fun decimalNumbers(){
        assertEquals(3.375, calcualVolumAire(1.5,1.5,1.5))
    }
    @Test
    fun positiveNumbers(){
        assertEquals(150.0, calcualVolumAire(3.0,5.0,10.0))
    }
}