import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_13KtTest{
    @Test
    fun lowRise(){
        assertEquals(10.2, temperatureRise(10.0,.2))
    }
    @Test
    fun riseBiggerThanTemperature(){
        assertEquals(22.0, temperatureRise(10.0,12.0))
    }
    @Test
    fun decimalTemperatureAndRise(){
        assertEquals(24.9, temperatureRise(10.4, 14.5))
    }
}