import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_9KtTest{
    @Test
    fun basicResult(){
        assertEquals(20.0, discountResult(100.0,80.0))
    }
    @Test
    fun doubleNumbers(){
        assertEquals(15.0, discountResult(10.0, 8.5))
    }
    @Test
    fun actualPriceSameOriginalPrice(){
        assertEquals(0.0,discountResult(100.0,100.0))
    }
}