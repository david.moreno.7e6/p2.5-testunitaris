import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_10KtTest{
    @Test
    fun longDecimalExpectedNumbers(){
        assertEquals(1134.1149479459152, pizzaSuperficie(-38.0))
    }
    @Test
    fun lowNumber(){
        assertEquals(Math.PI, pizzaSuperficie(2.0))
    }
    @Test
    fun decimalNumber(){
        assertEquals(5.3092915845667505, pizzaSuperficie(2.6))
    }

}