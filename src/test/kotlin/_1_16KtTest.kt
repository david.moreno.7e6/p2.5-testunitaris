import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_16KtTest{
    @Test
    fun negativeNumber(){
        assertEquals(-25.0, intToDouble(-25))
    }
    @Test
    fun zeroNumber(){
        assertEquals(0.0, intToDouble(0))
    }
    @Test
    fun positiveNumber(){
        assertEquals(48.0, intToDouble(48))
    }
}