import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_2KtTest{

    @Test
    fun checkNegativeNumber(){
        assertEquals(-2, doubleValue(-1))
    }
    @Test
    fun checkZeroNumber(){
        assertEquals(0, doubleValue(0))
    }
    @Test
    fun checkMultiplyNumber(){
        assertEquals(2, doubleValue(1))
    }
    @Test
    fun bigNumber () {
        assertEquals(1500, doubleValue(750))
    }
}