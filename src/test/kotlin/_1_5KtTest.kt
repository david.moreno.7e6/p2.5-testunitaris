import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_5KtTest{

    @Test
    fun negativeNumber(){
        assertEquals(-9, operacioBoja(4,5,-1,2))
    }
    @Test
    fun allNegativeNumber(){
        assertEquals(9, operacioBoja(-4,-5,-1,-2))
    }
    @Test
    fun positiveNumbers(){
        assertEquals(9, operacioBoja(4,5,1,2))
    }
}