import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_14KtTest{
    @Test
    fun dinnerPriceLow(){
        assertEquals(0.25, priceForEveryClient(4,1.0))
    }
    @Test
    fun clientBigNumber(){
        assertEquals(1.0, priceForEveryClient(10000,10000.0))
    }
    @Test
    fun dinnerPriceDecimal(){
        assertEquals(6.578, priceForEveryClient(5,32.89))
    }

}