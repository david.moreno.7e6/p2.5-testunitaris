import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_3KtTest {
    @Test
    fun negativeNumberPlusPositiveNumber(){
        assertEquals(0, sumaEnters(-1,1))
    }
    @Test
    fun negativeNumberSummary(){
        assertEquals(-10, sumaEnters(-6,-4))
    }
    @Test
    fun postiveNumberSummary(){
        assertEquals(10, sumaEnters(7, 3))
    }
}