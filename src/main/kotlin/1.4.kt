/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Calcual l'àrea
*/
import java.util.*
import kotlin.math.abs

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix l'amplada:")
    val amplada = scanner.nextInt()
    println("Aquesta és l'amplada: ")
    println(amplada)
    if (amplada==0)
    println("Introdueix la llargada:")
    val llargada = scanner.nextInt()
    println("Aquesta és la llargada: ")
    println(llargada)
    if (amplada> 0 && llargada > 0) {
        println("Area: ")
        print(calculArea(amplada, llargada))
    }
    else println("No existeix area")
}

fun calculArea (amplada: Int, llargada: Int): Int{
    return amplada* llargada
}