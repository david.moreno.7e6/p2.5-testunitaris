/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Afegeix un segon
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número de segons:")
    val segons = scanner.nextInt()
    println("Aquest és el número de segons: ")
    println(segons)
    if (segons<0 || segons>59){
        println("No pot haber segons negatius o superiors a 59")
    }
    else{
    println("El numero de segons resultants es : ")
    println((oneSecondMore(segons)))}
}

fun oneSecondMore (segons: Int): Int{
    return (segons+1)%60
}