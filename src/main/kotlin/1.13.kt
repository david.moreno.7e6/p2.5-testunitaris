/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Quina temperatura fa?
*/
import java.util.*
import kotlin.math.abs

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix una temperatura:")
    val Temperatura = scanner.nextDouble()
    println("Aquesta es la temperatura: ")
    println(Temperatura)
    println("Introdueix un augment:")
    val augment = scanner.nextDouble()
    println("Aquest és l'augment: ")
    println(augment)
    if (augment<=0) {println("La temperatura no ha augmentat")}
    else
    {
    println("La temperatura actual es: ")
    print(temperatureRise(Temperatura, augment))
    print('º')}
}

fun temperatureRise(Temperatura: Double, augment: Double):Double{
    return Temperatura + augment
}