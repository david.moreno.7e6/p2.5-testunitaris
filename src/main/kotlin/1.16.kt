/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Transforma l'enter
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero = scanner.nextInt()
    println("Aquest és el número introduit: ")
    println(numero)
    println("El resultat de pasar el numero a decimal es: ")
    println(intToDouble(numero))
}
fun intToDouble(numero: Int):Double{
    return numero.toDouble()
}