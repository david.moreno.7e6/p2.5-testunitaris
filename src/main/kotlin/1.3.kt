/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Suma de dos nombres enters
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val firstNumber = scanner.nextInt()
    println("Aquest és el número introduït: ")
    println(firstNumber)
    println("Introdueix un número:")
    val secondNumber = scanner.nextInt()
    println("Aquest és el número introduït: ")
    println(secondNumber)
    println("Resultat:")
    print(sumaEnters(firstNumber, secondNumber))
}

fun sumaEnters (num:Int,num2:Int): Int{
    return num+num2
}