import java.lang.Math.abs
import java.util.Scanner

/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Pupitres
*/

fun main() {
    val scanner= Scanner(System.`in`)
    println("Introdueix el nombre d'alumnes de la primer clase:")
    val primerNum= scanner.nextInt()
    println("Introdueix el nombre d'alumnes de la segona clase:")
    val segonNum= scanner.nextInt()
    println("Introdueix el nombre d'alumnes de la tercera clase:")
    val tercerNum= scanner.nextInt()
    if (primerNum<0|| segonNum<0||tercerNum<0){
        println("No pot haber una clase amb n negatius alumnes")
    }
    else {
        println("El nombre de pupitres es: ")
        print(pupitresForClass(primerNum, segonNum, tercerNum))}
}
fun pupitresForClass(primerNum:Int, segonNum: Int, tercerNum: Int):Int{
    return ((((primerNum) + segonNum + tercerNum)/2.0)+0.5).toInt()
}