/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Número següent
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número enter:")
    val numero = scanner1.nextInt()
    println("Resultat del seguent número: ")
    print(nextNumber(numero))
}
fun nextNumber (numero: Int):Int{
    return numero+1
}