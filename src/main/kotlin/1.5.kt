/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Operació boja
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix el primer número:")
    val firstNum = scanner.nextInt()
    println("Aquest és el número introduït: ")
    println(firstNum)
    println("Introdueix el segon número:")
    val secondNum = scanner.nextInt()
    println("Aquest és el número introduït: ")
    println(secondNum)
    println("Introdueix el tercer número:")
    val thirdNum = scanner.nextInt()
    println("Aquest és el número introduït: ")
    println(thirdNum)
    println("Introdueix el cuart número:")
    val fourthNum = scanner.nextInt()
    println("Aquest és el número introduït: ")
    println(fourthNum)
    println("Resultat: "); print(operacioBoja(firstNum, secondNum, thirdNum, fourthNum))
}

fun operacioBoja (firstNum: Int, secondNum: Int, thirdNum: Int, fourthNum: Int): Int{
    return (firstNum+secondNum)*(thirdNum%fourthNum)
}