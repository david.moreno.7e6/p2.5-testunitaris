/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Calculadora de volum d'aire
*/
import java.util.*
import kotlin.math.abs

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix la llargada:")
    val llargada = scanner.nextDouble()
    println("Aquesta és la llargada: ")
    println(llargada)
    println("Introdueix l'amplada:")
    val amplada = scanner.nextDouble()
    println("Aquesta és l'amplada: ")
    println(amplada)
    println("Introdueix l'alçada:")
    val alçada = scanner.nextDouble()
    println("Aquest és l'alçada: ")
    println(alçada)
    if (llargada<=0.0 || amplada<=0.0 || alçada<=0.0) {println("Es imposible calcular el volum")}
    else{
        println("El volum es:")
        print(calcualVolumAire(llargada, amplada, alçada))}
}

fun calcualVolumAire(llargada: Double, amplada: Double, alçada: Double): Double{
    return llargada* amplada* alçada
}