/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Divisor de compte
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número de comensals:")
    val Comensals = scanner.nextInt()
    println("Aquest és el número de comensals: ")
    println(Comensals)
    println("Introdueix el preu del sopar:")
    val PreuSopar = scanner.nextDouble()
    println("Aquest és el preu del sopar: ")
    println(PreuSopar)
    if (PreuSopar<=0 || Comensals<=0){
        println("Aquestes dades no son correctes")
    }
    else{
    println("Aquest és el preu per comensal: ")
    print(priceForEveryClient(Comensals, PreuSopar))
    print('€')}
}

fun priceForEveryClient (Comensals: Int, PreuSopar: Double): Double{
    return PreuSopar / Comensals
}