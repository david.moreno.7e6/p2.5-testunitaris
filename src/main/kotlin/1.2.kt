/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Dobla l'enter
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val userInputValue = scanner.nextInt()
    println("Resultat: ")
    print(doubleValue(userInputValue))
}

fun doubleValue (numero: Int): Int{
    return numero*2
}