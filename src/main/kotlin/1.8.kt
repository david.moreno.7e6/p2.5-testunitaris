/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Dobla el decimal
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número decimal:")
    val numero = scanner1.nextDouble()
    println("Resultat de doblar el decimal: ")
    println(doubleDecimalValue(numero))
}

fun doubleDecimalValue (numero: Double): Double{
    return numero*2
}