/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: De Celsius a Fahrenheit
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix els graus en Celsius:")
    val Celsius = scanner.nextDouble()
    println("Aquest és el número de graus Celsius introduïts: ")
    println(Celsius)
    println("Aquest és el número de graus Fahrenheit: ")
    println(celsiusToFahrenheit(Celsius))
}

fun celsiusToFahrenheit(Celsius: Double): Double{
    return Celsius * 1.8 + 32
}