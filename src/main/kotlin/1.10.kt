/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Quina es la mida de la meva pizza?
*/
import java.util.*
import kotlin.math.abs

fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix el diametre:")
    val diametro = scanner1.nextDouble()
    println("Aquest és el diametre introduït: ")
    println(diametro)
    if (diametro<=0.0){ println("La pizza no existeix...")}
    else{
        println("La superficie de la pizza es:")
    println(pizzaSuperficie(diametro))}
}

fun pizzaSuperficie(diametro: Double):Double{
    return Math.PI * ((diametro* diametro) / 4)
}