/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Calcula el descompte
*/
import java.util.*
import kotlin.math.abs

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix el valor del preu original:")
    val preuOriginal = scanner.nextDouble()
    println("Aquest és el valor introduït: ")
    println(preuOriginal)
    println("Introdueix uel valor del preu actual:")
    val preuActual = scanner.nextDouble()
    println("Aquest és el valor introduït: ")
    println(preuActual)
    if (preuActual>preuOriginal){ println("No hi ha descompte... HAN PUJAT EL PREU!")}
    else if (preuActual<=0|| preuOriginal<=0){
        println("No pot haber un preu negatiu ni igual a 0")
    }
    else {
    println("El percentatge de descompte es: ")
    println(discountResult(preuOriginal, preuActual))}
}

fun discountResult (preuOriginal: Double, preuActual: Double): Double{
    return ((preuOriginal- preuActual)/ preuOriginal)*100
}